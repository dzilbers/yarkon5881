﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class Contact
    {
        string name, phone;
        public string GetName() => name;
        public string GetPhone() => phone;
        public void SetName(string name) => this.name = name;
        public void SetPhone(string phone) => this.phone = phone;
        public Contact(string name, string phone)
        {
            this.name = name;
            this.phone = phone;
        }
    }
}
