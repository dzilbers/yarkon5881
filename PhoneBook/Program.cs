﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class Program
    {
        enum Menu { A, B, C, D };
        static void Main(string[] args)
        {
            char c = (char)Console.Read();
            int n;
            Console.Write("Enter a number: ");
            while (!int.TryParse(Console.ReadLine(), out n))
               Console.Write("Wrong input, enter a number again: ");
            Menu choice = (Menu)n;
            switch (choice)
            {
                case Menu.A:
                    break;
                case Menu.B:
                    break;
                case Menu.C:
                    break;
                default:
                    break;
            }

        }
    }
}
