﻿using Recursion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class PhoneBook
    {
        Contact[] contacts = new Contact[0];
        ContactNode firstContact = null;

        public void AddContactOld(string name, string phone)
        {
            for (int i = 0; i < contacts.Length; ++i)
                if (name.Equals(contacts[i].GetName()))
                {
                    contacts[i].SetPhone(phone);
                    return;
                }

            Contact[] newContacts = new Contact[contacts.Length + 1];

            int index = 0;
            for (; index < contacts.Length && name.CompareTo(contacts[index].GetName()) > 0; ++index)
                newContacts[index] = contacts[index];

            newContacts[index] = new Contact(name, phone);

            for (; index < contacts.Length; ++index)
                newContacts[index + 1] = contacts[index];
        }

        public void AddContactNew(string name, string phone)
        {
            ContactNode newContact = new ContactNode(new Contact(name, phone), firstContact);
            firstContact = newContact;
        }
        public bool DelContactNew(string name)
        {
            ContactNode previous = null;
            for (ContactNode node = firstContact; node != null; node = node.GetNext())
            {
                if (name.Equals(node.GetValue().GetName()))
                {
                    if (previous == null) // remove first node
                        firstContact = node.GetNext();
                    else // remove node in the middle
                        previous.SetNext(node.GetNext());
                    node.SetNext(null);
                    return true;
                }
                previous = node;
            }
            return false;
        }

        public void PrintContacts1()
        {
            ContactNode contact = firstContact;
            while (contact != null)
            {
                Console.WriteLine(contact.GetValue());
                contact = contact.GetNext();
            }
        }

        public void PrintContacts2() => PrintContacts2(firstContact);
        private void PrintContacts2(ContactNode contact)
        {
            if (contact == null) return;
            Console.WriteLine(contact.GetValue());
            PrintContacts2(contact.GetNext());
        }

        private void SwapContacts1(int index1, int index2)
        {
            Contact temp = new Contact(contacts[index2].GetName(), contacts[index2].GetPhone());
            contacts[index2].SetName(contacts[index1].GetName());
            contacts[index2].SetPhone(contacts[index1].GetPhone());
            contacts[index1].SetName(temp.GetName());
            contacts[index1].SetPhone(temp.GetPhone());
        }
        private void SwapContacts2(int index1, int index2)
        {
            Contact temp = contacts[index2];
            contacts[index2] = contacts[index1];
            contacts[index1] = temp;
        }


    }
}
