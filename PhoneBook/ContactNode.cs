﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class ContactNode
    {
        Contact value;
        ContactNode next;

        public ContactNode(Contact value)
        {
            this.value = value;
            this.next = null;
        }
        public ContactNode(Contact value, ContactNode next)
        {
            this.value = value;
            this.next = next;
        }

        public Contact GetValue() => value;
        public void SetValue(Contact value) => this.value = value;
        public ContactNode GetNext() => next;
        public void SetNext(ContactNode next) => this.next = next;

        public bool HasNext() => next != null;

        public override string ToString() => value.ToString();
    }
}
