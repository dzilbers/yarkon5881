﻿using System;

namespace Recursion
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(Mult(4, 5));
            //PrintDigits(1234567890);
            int[] nums = { 2, 6, 8, 10, 14, 17, 26, 37 };
            // using interpolated string
            Console.WriteLine($"Number 17 is found at index {Find(nums, 17)}");
            //Console.WriteLine("Number 17 is found at index {0}", Find(nums, 17));
            //Console.WriteLine("Number 17 is found at index " + Find(nums, 17));
        }

        static void PrintDigits(int n)
        {
            if (n < 10) // מקרה בסיס \ תנאי עצירה
            {
                Console.WriteLine(n);
                return;
            }
            Console.WriteLine(n % 10); // 0
            PrintDigits(n / 10); // 123456789
        }

        static int Mult(int n, int m)
        {
            if (m == 0)
                return 0;
            return Mult(n, m - 1) + n;
        }

        public static int Find(int[] a, int num)
        {
            return Find(a, num, 0, a.Length - 1);
        }

        private static int Find(int[] a, int num, int b, int e)
        {
            if (b > e) return -1;
            int m = (b + e) / 2;
            if (num == a[m]) return m;
            if (num < a[m]) return Find(a, num, b, m - 1);
            else return Find(a, num, m + 1, e);
        }
    }
}
