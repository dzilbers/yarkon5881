﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedLists
{
    class IntNode
    {
        int value;
        IntNode next;

        public IntNode(int value)
        {
            this.value = value;
            this.next = null;
        }
        public IntNode(int value, IntNode next)
        {
            this.value = value;
            this.next = next;
        }

        public int GetValue()
        {
            return value;
        }
        public void SetValue(int value)
        {
            this.value = value;
        }
        public IntNode GetNext()
        {
            return next;
        }
        public void SetNext(IntNode next)
        {
            this.next = next;
        }

        public bool HasNext()
        {
            return next != null;
        }

        public override string ToString()
        {
            if (next == null)
                return " " + value.ToString() + " --|";
            else
                return " " + value.ToString() + " -> " + next;
        }
    }
}
