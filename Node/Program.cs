﻿using Node;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace LinkedLists
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //Node<int> lst = new Node<int>(6);
        //AddToListAfterFirst(lst, 9);
        //AddToListAfterFirst(lst, 5);
        //// Add to the start of list
        //Node<int> node1 = new Node<int>(7, lst);
        //lst = node1;

        //lst = new Node<int>(12, lst);


        //// delete 2nd node
        //Node<int> del = lst.GetNext();
        //lst.SetNext(del.GetNext());
        //del.SetNext(null);

        //lst.SetNext(lst.GetNext().GetNext());

        //Node<string> names =
        //    new Node<string>("Dani",
        //    new Node<string>("Yossi",
        //    new Node<string>("Ruvi",
        //    new Node<string>("Shimi"
        //    ))));

        //Node<Point> square =
        //    new Node<Point>(new Point(0, 0),
        //    new Node<Point>(new Point(0, 1),
        //    new Node<Point>(new Point(1, 1),
        //    new Node<Point>(new Point(1, 0)
        //    ))));
        //Console.Write("Square: ");
        // PrintList<Point>(square);
        //PrintList(square);
        //PrintCharCounts();
        //}

        public static void PrintList<U>(Node<U> list)
        {
            if (list == null)
                Console.WriteLine();
            else
            {
                Console.Write(list);
                PrintList(list.GetNext());
            }
        }

        public static bool CheckValue<T>(Node<T> list, T value)
        {
            if (list == null) return false;
            if (list.GetValue().Equals(value))
                return true;
            return CheckValue(list.GetNext(), value);
        }

        public static void AddToListAfterFirst(Node<int> list, int value)
        {
            Node<int> node = new Node<int>(value, list.GetNext());
            list.SetNext(node);
        }

        public static IntNode RemoveFromListStart(IntNode list)
        {
            IntNode temp = list.GetNext();
            list.SetNext(null);
            return temp;
        }

        public static void AddAfterNode1(IntNode node, int value)
        {
            IntNode add = new IntNode(value, node.GetNext());
            node.SetNext(add);
        }
        public static void AddAfterNode2(IntNode node, int value)
        {
            node.SetNext(new IntNode(value, node.GetNext()));
        }

        public static void RemoveNextNode(IntNode node)
        {
            IntNode del = node.GetNext();
            node.SetNext(del.GetNext());
            del.SetNext(null);
        }

        public static void RemoveNode(IntNode list, IntNode node)
        { // the deleted node must not be the first one in the list!
            while (list.GetNext() != node)
                list = list.GetNext();
            RemoveNextNode(list);
        }

        public static void IncrementList1(IntNode list)
        {
            while (list != null)
            {
                list.SetValue(list.GetValue() + 1);
                list = list.GetNext();
            }
        }
        public static void IncrementList2(IntNode list)
        {
            if (list != null)
            {
                list.SetValue(list.GetValue() + 1);
                IncrementList2(list.GetNext());
            }
        }

        public static int SumList1(IntNode list)
        {
            int sum = 0;
            while (list != null)
                sum += list.GetValue();
            return sum;
        }
        public static int SumList2(IntNode list)
        {
            if (list == null) return 0;
            return list.GetValue() + SumList2(list.GetNext());
        }
        public static int SumList3(IntNode list)
        {
            return list == null ? 0 : list.GetValue() + SumList3(list.GetNext());
        }

        // The list must not be empty
        public static void AddToListAtEnd(IntNode list, int value)
        {
            if (list.HasNext())
                AddToListAtEnd(list.GetNext(), value);
            else
                list.SetNext(new IntNode(value));
        }

        static Random rnd = new Random();
        public static IntNode BuildRandomList1(int count, int from, int to)
        { // adding at the start
            IntNode lst = null;
            for (int i = count; i > 0; --i)
                lst = new IntNode(rnd.Next(from, to), lst);
            return lst;
        }
        public static IntNode BuildRandomList2(int count, int from, int to)
        { // adding at the end
            IntNode first = new IntNode(rnd.Next(from, to));
            IntNode last = first;
            for (int i = count - 1; i > 0; --i)
            {
                IntNode temp = new IntNode(rnd.Next(from, to));
                last.SetNext(temp);
                last = temp;
            }
            return first;
        }

        public static IntNode AddNodeToOrderedList(IntNode list, int value)
        {
            if (list == null) return new IntNode(value);
            if (value < list.GetValue()) return new IntNode(value, list);
            IntNode curr = list;
            while (curr.HasNext() && value > curr.GetNext().GetValue())
                curr = curr.GetNext();
            AddAfterNode2(curr, value);
            return list;
        }


        // פרטני איתי ואייל
        // Count list of integers members by recursion\
        public static int CountMembers(IntNode list)
        {
            if (list == null)
                return 0;
            else
                return 1 + CountMembers(list.GetNext());
        }

        public static int CountChar(Node<char> list, char ch)
        {
            if (list == null) return 0;
            int count = CountChar(list.GetNext(), ch);
            if (list.GetValue() == ch) ++count;
            return count;
        }

        public static void PrintCharCounts()
        {
            string line = Console.ReadLine();
            Node<char> list = new Node<char>(line[0]);
            for (int i = 1; i < line.Length; ++i)
                list = new Node<char>(line[i], list);

            for (char ch = 'A'; ch <= 'Z'; ++ch)
            {
                int count = CountChar(list, ch);
                if (count > 0)
                    Console.WriteLine(ch + ": " + count);
            }
            for (char ch = 'a'; ch <= 'z'; ++ch)
            {
                int count = CountChar(list, ch);
                if (count > 0)
                    Console.WriteLine(ch + ": " + count);
            }
        }

        public static int[] GetCharCounts(Node<char> list)
        {
            int[] letterCounts = new int[26 * 2];
            for (char ch = 'A'; ch <= 'Z'; ++ch)
                letterCounts[ch - 'A'] = CountChar(list, ch);
            for (char ch = 'a'; ch <= 'z'; ++ch)
                letterCounts[ch - 'a' + 26] = CountChar(list, ch);
            return letterCounts;
        }

        public static void PrintCharCountsOther()
        {
            string line = Console.ReadLine();
            Node<char> list = new Node<char>(line[0]);
            for (int i = 1; i < line.Length; ++i)
                list = new Node<char>(line[i], list);

            int[] counts = GetCharCounts(list);

            for (int i = 0; i < counts.Length; ++i)
            {
                if (i < 26)
                    Console.Write((char)(i + 'A'));
                else
                    Console.Write((char)(i - 26 + 'a'));
                Console.WriteLine(": " + counts[i]);
            }
        }

        static public bool CheckLoop1<T>(Node<T> list) // O(N^2) :סיבוכיות
        {
            Node<Node<T>> help;
            help = new Node<Node<T>>(list);
            list = list.GetNext();

            while (list != null)
            {
                // Check if th the list node is one of the old ones
                for (Node<Node<T>> curr = help; curr != null; curr = curr.GetNext())
                    if (curr.GetValue() == list) return true;
                help = new Node<Node<T>>(list, help);
                list = list.GetNext();
            }
            return false;
        }

        static public bool CheckLoop2<T>(Node<T> list) // O(N) :סיבוכיות
        {
            Node<T> turtle = list;
            Node<T> hare = list.GetNext();
            while (hare != null)
            {
                if (hare == turtle) return true;
                turtle = turtle.GetNext();
                hare = hare.GetNext();
                if (hare == null) break;
                hare = hare.GetNext();
            }
            return false;
        }

        static void Main(string[] args)
        {
            Node<int> list1 = new Node<int>(1,
                              new Node<int>(2,
                              new Node<int>(3,
                              new Node<int>(4))));
            Console.WriteLine("List 1: " + CheckLoop2(list1));

            Node<int> list2 = new Node<int>(1,
                              new Node<int>(2,
                              new Node<int>(3,
                              new Node<int>(4,
                              new Node<int>(5)))));
            int count = 0;
            Node<int> middle = null;
            for (Node<int> node = list2; node != null; node = node.GetNext())
            {
                ++count;
                if (count == 3)
                    middle = node;
                if (node.GetNext() == null)
                {
                    node.SetNext(middle);
                    break;
                }
            }
            Console.WriteLine("List 2: " + CheckLoop2(list2));
        }

        Node<int> RemoveValueFromList(Node<int> list, int value)
        {
            if (list == null) return null; // בדיקה האם קיבלנו רשימה ריקה

            if (list.GetValue() == value) // האם החוליה שצריך למחוק היא הראשונה
                return list.GetNext(); // להחזיר חוליה שניה שתהיה הראשונה החדשה

            Node<int> prev = list; // נתחיל מהחוליה הראשונה ונגיע לחוליה שלפני זו שצריך למחוק
            // כל עוד יש חוליה אחרי זה ובחוליה שאחרי זה - לא הערך שצריך למחוק
            while (prev.HasNext() && prev.GetNext().GetValue() != value)
                prev = prev.GetNext(); // נתקדם לחוליה הבאה
            if (prev.HasNext()) // עברנו על כל הרשימה ומצאנו את הערך שצריך למחוק
                // אם מצאנו - בוא נמחק אותו - נדלג מהחוליה הנוכחית שהבאה תהיה אחרי החוליה הנמחקת
                prev.SetNext(prev.GetNext().GetNext());
            return list; // נחזיר הפניה לרחוליה הראשונה ברשימה
        }
        // Main(...)
        // {
        //     Node<int> list = ...; // נבנה את הרשימה
        //     list = RemoveValueFromList(list, 8);
        //     ...
        // }
    }

    static class Tools
    {
        public static string TString<T>(this T[] arr)
        {
            if (arr.Length == 0) return "[]";
            string str = "[" + arr[0];
            for (int i = 1; i < arr.Length; ++i)
                str += "," + arr[i];
            return str + "]";
        }
    }
}
