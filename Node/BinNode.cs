﻿namespace LinkedLists
{
    class BinNode<T>
    {
        T value;
        BinNode<T> left;
        BinNode<T> right;

        public BinNode(T value)
        {
            this.value = value;
            this.left = null;
            this.right = null;
        }
        public BinNode(T value, BinNode<T> left, BinNode<T> right)
        {
            this.value = value;
            this.left = left;
            this.right = right;
        }

        public T GetValue()
        {
            return value;
        }
        public void SetValue(T value)
        {
            this.value = value;
        }
        public BinNode<T> GetLeft()
        {
            return left;
        }
        public void SetLeft(BinNode<T> left)
        {
            this.left = left;
        }

        public BinNode<T> GetRight()
        {
            return right;
        }
        public void SetRight(BinNode<T> right)
        {
            this.right = right;
        }

        public bool HasLeft()
        {
            return left != null;
        }
        public bool HasRight()
        {
            return right != null;
        }

        public override string ToString()
        {
            string str = "";

            if (left == null)
                str += "|--";

            str += " " + value.ToString();

            if (right == null)
                str += " --|";
            else
                str += " <=> ";

            return str;
        }
    }
}
