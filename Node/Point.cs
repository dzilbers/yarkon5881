﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Node
{
    class Point
    {
        private double x, y;

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
        double GetX() => x;
        double GetY() => y;
        void SetX(double x) => this.x = x;
        void SetY(double y) => this.y = y;

        public override string ToString() => $"({x},{y})";
    }
}
