﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinkedLists;

namespace Test2Q5
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        public static Node<int> FindMins(Node<int> list)
        {
            if (!list.HasNext())
                return null;

            // אם תת-רשימה ריקה - תחזיר את התוצאה של ההמשך
            if (list.GetNext().GetValue() == 0)
                return FindMins(list.GetNext());

            Node<int> prevNode = list; // חוליה שלפני החוליה עם ערך מינימלי
            list = list.GetNext(); // נתקדם לחוליה הבאה שהיא עם ערך לא אפס
            int min = list.GetValue(); // הערך הזה הוא בינתיים המינימלי
            // כל עוד החוליה הבאה היא לא סוף תת הרשימה הנבדקת
            while (list.GetNext().GetValue() != 0)
            {
                // נבדדוק האם בחוליה הבאה הערך קטן יותר ממה שמצאנו עד כה
                if (list.GetNext().GetValue() < min)
                {
                    prevNode = list; // עדכון של חוליה שקודמת לחוליה המיניימלית
                    min = list.GetNext().GetValue(); // ונשמור את הערך המינימלי
                }
                list = list.GetNext(); // נתקדם לחוליה הבאה
            }

            // מחיקת החוליה המינימלית שמצאנו
            prevNode.SetNext(prevNode.GetNext().GetNext());
            // נחזיר רשימה עם החוליה של ערך מינימךי שמצאנו ורשימת המינמומים של תת הרשימות הבאות
            return new Node<int>(min, FindMins(list.GetNext()));
        }


        public static void FindMins2(Node<int> list, Node<int> mins)
        {
            //...
            int min = 10;
            mins.SetNext(new Node<int>(min));
            mins = mins.GetNext();
            FindMins2(list.GetNext(), mins);
            //...
        }

        public static Node<int> FindMins2(Node<int> list)
        {
            Node<int> mins = new Node<int>(0); // fake node
            FindMins2(list, mins);
            return mins.GetNext(); // without the fake node of course
        }
    }
}
