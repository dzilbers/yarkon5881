﻿using System;
using LinkedLists;

namespace AbstractTypes
{
    public class Queue<T>
    {
        Node<T> first;
        Node<T> last;
        public Queue()
        {
            first = null;
            last = null;
        }
        public bool IsEmpty()
        { 
            return first == null; 
        }
        public void Insert(T value)
        {
            if (first == null)
            {
                first = last = new Node<T>(value);
            }
            else
            {
                last.SetNext(new Node<T>(value));
                last = last.GetNext();
            }
        }
        public T Head()
        {
            return first.GetValue();
        }
        public T Remove()
        {
            Node<T> temp = first;
            first = first.GetNext();
            temp.SetNext(null);
            if (first == null)
                last = null;
            return temp.GetValue();
        }
        public override string ToString()
        {
            string str = "<= ";
            for (Node<T> p = first; p != null; p = p.GetNext())
                str += p.GetValue() + " ";
            return str + "<=";
        }
    }
}
