﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<double> qd = new Queue<double>();
            qd.Insert(double.PositiveInfinity);

            Queue<int> q = new Queue<int>();
            q.Insert(1);
            q.Insert(2);
            q.Insert(3);
            q.Insert(4);
            q.Insert(5);
            Console.WriteLine(q);
            Console.WriteLine(Mystery1(q));
            Console.WriteLine(q);

            Stack<int> st1 = new Stack<int>();
            st1.Push(4);
            st1.Push(2);
            st1.Push(9);
            Console.WriteLine(st1);

            Console.WriteLine(CheckParenthesis("(2+3-[1*3])+{3*2}"));
            Console.WriteLine(CheckParenthesis("(2+3-[1*3]))+{3*2}"));
            Console.WriteLine(CheckParenthesis("(2+3-[1*3]+{3*2}"));
            Console.WriteLine(CheckParenthesis("(2+3-[1*3)]+{3*2}"));
        }

        public static int QueueLength1<T>(Queue<T> q)
        {
            Queue<T> copy = new Queue<T>();
            int count = 0;
            while (!q.IsEmpty())
            {
                copy.Insert(q.Remove());
                ++count;
            }

            // נעתיק בחזרה מהעתק לתור המקורי
            while (!copy.IsEmpty())
            {
                q.Insert(copy.Remove());
            }

            return count;
        }

        public static void ReverseQueue<T>(Queue<T> q)
        {
            if (q.IsEmpty()) return;
            T temp = q.Remove();
            ReverseQueue(q);
            q.Insert(temp);
        }

        public static int QueueLength2<T>(Queue<T> q)
        {
            int count = 0;
            T dummy = default(T);
            q.Insert(dummy);
            while (!q.Head().Equals(dummy))
            {
                q.Insert(q.Remove());
                count++;
            }
            q.Remove();
            return count;
        }

        public static Queue<T> CloneQueue<T>(Queue<T> q)
        {
            Queue<T> copy = new Queue<T>();
            Queue<T> temp = new Queue<T>();
            while (!q.IsEmpty())
                temp.Insert(q.Remove());

            // נעתיק בחזרה מהעתק לתור המקורי
            T item;
            while (!temp.IsEmpty())
            {
                item = temp.Remove();
                q.Insert(item);
                copy.Insert(item);
            }

            return copy;
        }

        public static bool Mystery1(Queue<int> q)
        {
            if (q.IsEmpty()) return true;

            int x = q.Remove();
            if (q.IsEmpty()) return false;

            int y = q.Remove();
            bool b = Mystery1(q);
            q.Insert(y);
            q.Insert(x);

            return b;
        }

        // to אל ראש המסנית  from הפעולה תעביר בסדר הפוך את כל התוכן של מחסנית
        // :למשל אם תוכן המחסניות לפני זימון הפעולה
        // from: [1,2,3[     to: [4, 5[
        // :אחרי הזימון יהיה
        // from: [1,2,3,5,4[     to: [  (empty)
        public static void RevertStack<T>(Stack<T> from, Stack<T> to)
        {
            while (!from.IsEmpty())
                to.Push(from.Pop());
        }

        // to אל ראש המסנית  from הפעולה תעביר באותו את כל התוכן של מחסנית
        // :למשל אם תוכן המחסניות לפני זימון הפעולה
        // from: [1,2,3[     to: [4, 5[
        // :אחרי הזימון יהיה
        // from: [1,2,3,4,5[     to: [  (empty)
        public static void PutOnTop<T>(Stack<T> from, Stack<T> to)
        {
            Stack<T> temp = new Stack<T>();
            RevertStack(from, temp);
            RevertStack(temp, to);
        }

        // תרגיל בדיקת הסוגריים בביטוי מתמטי
        public static bool CheckParenthesis(string expression)
        {
            Stack<char> stk = new Stack<char>();
            foreach (char ch in expression)
            // for (int i = 0; i < expression.Length; ++i) { char ch = expression[i]; ...
            {
                switch (ch)
                {
                    case '(':
                    case '[':
                    case '{':
                        stk.Push(ch);
                        break;
                    case ')':
                        if (stk.Pop() != '(')
                            return false;
                        break;
                    case ']':
                        if (stk.Pop() != '[')
                            return false;
                        break;
                    case '}':
                        if (stk.Pop() != '{')
                            return false;
                        break;
                }
            }
            return stk.IsEmpty();
        }

        static Dictionary<char, char> parenthesis = new Dictionary<char, char>()
                                                        { { ')', '(' }, { ']', '[' }, { '}', '{' } };
        public static bool CheckParenthesisAdvanced(string expression)
        {
            Stack<char> stk = new Stack<char>();
            foreach (char ch in expression)
            // for (int i = 0; i < expression.Length; ++i) { char ch = expression[i]; ...
            {
                switch (ch)
                {
                    case '(':
                    case '[':
                    case '{':
                        stk.Push(ch);
                        break;
                    case ')':
                    case ']':
                    case '}':
                        if (stk.IsEmpty() || stk.Pop() != parenthesis[ch])
                            return false;
                        break;
                }
            }
            return stk.IsEmpty();
        }
    }
}
