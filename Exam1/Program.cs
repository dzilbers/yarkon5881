﻿using System;
using AbstractTypes;
using LinkedLists;

namespace Exam1
{
    class Program
    {
        static void Main(string[] args)
        {
            Book b1 = new Book(1122, 1981, true);
            Book b2 = new Book(2233, 2015, false);

            if (b1.Years(2014) < 10 || b1.GetIsTranslated())
                Console.WriteLine("***");
            else
                Console.WriteLine("+++");
            if (b1.Years(2020) < 6)
                Console.WriteLine("old");
            else if (b2.GetIsTranslated())
                Console.WriteLine("English");
        }

        public static int What(Queue<int> q)
        {
            int x = q.Remove();
            int y = q.Head();
            q.Insert(x);
            if (x > y)
                return 0;
            return What(q) + (y - x);
        }

        public static int Go(int[] arr, int x, int y)
        {
            if (x == y) return arr[x] % 10;
            if (arr[x] % 10 < arr[y] % 10)
                    return Go(arr, x + 1, y);
            return Go(arr, x, y - 1);
        }
    
        public static Node<int> Check(Node<int> l1, Node<int> l2)
        {
            return Check(l1, l2, null);
        }
        public static Node<int> Check(Node<int> l1, Node<int> l2, Node<int> list)
        {
            return null;
        }
    }

    class Book
    {
        private int code;
        private int year;
        private bool isTranslated;
        public Book(int code, int year, bool isTranslated)
        { }
        public int GetCode() => code;
        public int GetYear() => year;
        public bool GetIsTranslated() => isTranslated;

        public int Years(int year1)
        { return 0; }
    }

    class Cake
    {
        private string name;
        private string[] ingredients;
        private int n;
        private bool gluten;
        private double price;

        public Cake(string name, double price)
        {
            this.name = name;
            this.ingredients = new string[20];
            this.n = 0;
            this.gluten = false;
            this.price = price;
        }

        public string GetName() => name;
        public string[] GetIngredients() => ingredients;
        public int GetN() => n;
        public bool GetGluten() => gluten;
        public double GetPrice() => price;
        public void SetName(string value) => name = value;
        public void SetIngredients(string[] value) => ingredients = value;
        public void SetN(int value) => n = value;
        public void SetGluten(bool value) => gluten = value;
        public void SetPrice(double value) => price = value;
        //...
    }
}
