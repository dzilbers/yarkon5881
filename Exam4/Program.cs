﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam4
{
    class Program
    {
        // Q1-A
        public static double AverageHigh(double j1, double j2, double j3)
        {
            return (j1 + j2 + j3) / 3;
        }

        static void Main(string[] args)
        {
            // Q1-B
            Console.WriteLine("Enter amount of jumpers: "); // לא חובה
            int n = int.Parse(Console.ReadLine());
            string bestName = "";
            double bestJ1 = 0, bestJ2 = 0, bestJ3 = 0;
            double bestAverage = 0;
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Enter jumper name: "); // לא חובה
                string name = Console.ReadLine();
                Console.WriteLine("Enter 1st jump: "); // לא חובה
                double j1 = double.Parse(Console.ReadLine());
                Console.WriteLine("Enter 2nd jump: "); // לא חובה
                double j2 = double.Parse(Console.ReadLine());
                Console.WriteLine("Enter 3rd jump: "); // לא חובה
                double j3 = double.Parse(Console.ReadLine());
                double average = AverageHigh(j1, j2, j3);
                if (average > bestAverage)
                {
                    bestName = name;
                    bestJ1 = j1;
                    bestJ2 = j2;
                    bestJ3 = j3;
                    bestAverage = average;
                }
            }
            Console.WriteLine("Winner name:" + bestName);
            Console.WriteLine("Winner 1st jump:" + bestJ1);
            Console.WriteLine("Winner 2nd jump:" + bestJ2);
            Console.WriteLine("Winner 3rd jump:" + bestJ3);
        }
    }
}

