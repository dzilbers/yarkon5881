﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test2Q2
{
    class StudLes
    {
        string name;
        string phone;
        int day; // 1 - 5
        public string GetName() => name;
        public int GetDay() => day;
    }

    class Student
    {
        StudLes[] arr = new StudLes[12]; // list of lessons
        int[] count = new int[5]; // number of lessons in day : indexes 0 - 4 => index = day-1

        public int HowManyStuds1()
        {
            int count = 0;
            for (int i = 0; i < 12 /* arr.Length */; ++i)
                if (this.arr[i] != null)
                    ++count;
            return count;
        }
        public int HowManyStuds2()
        {
            int counter = 0;
            for (int i = 0; i < 4; ++i)
                counter += this.count[i];
            return counter;
        }

        // assuming all null's are in the end of arr
        public void AddStudent1(StudLes stud)
        {
            int k = HowManyStuds1();
            if (k == 12)
            {
                Console.WriteLine("No place for more lessons");
                return;
            }
            arr[k] = stud;
            count[stud.GetDay() - 1]++;
        }

        public void AddStudent2(StudLes stud)
        {
            for (int i = 0; i < 12; ++i)
            {
                if (arr[i] == null)
                {
                    arr[i] = stud;
                    count[stud.GetDay() - 1]++;
                    return;
                }
            }
            Console.WriteLine("No place for more lessons");
        }

        public void CancelLessons(int day)
        {
            for (int i = 0; i < 12; ++i)
            {
                if (arr[i] != null && arr[i].GetDay() == day)
                {
                    Console.WriteLine(arr[i].GetName());
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
