﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YiftachAndTal
{
    class Program
    {
        static void Main(string[] args)
        {
            Bottle('$', 5);
        }

        public static void Bottle(char letter, int n, int blanks = 0)
        {
            BottleLine(letter, n, blanks);
            if (n == 1) return;
            Bottle(letter, n - 2, blanks + 1);
            BottleLine(letter, n, blanks);
        }

        private static void BottleLine(char letter, int n, int blanks)
        {
            for (int i = blanks; i > 0; --i)
                Console.Write(" ");
            for (int i = n; i > 0; --i)
                Console.Write(letter);
            Console.WriteLine();
        }


        private static void PrintDigits(int n)
        {
            if (n < 10)
                Console.WriteLine(n);
            else
            {
                Console.WriteLine(n % 10);
                PrintDigits(n / 10);
            }
        }
    }
}
